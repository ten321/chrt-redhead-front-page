# CHRT Front Page 
**Contributors:** cgrymala  
**Donate link:** http://www.chrt.org/  
**Requires at least:** 4.7  
**Tested up to:** 4.9.9  
**Stable tag:** 0.3  
**License:** GPL v2 or later  
**License URI:** http://www.gnu.org/licenses/old-licenses/gpl-2.0.html  

Implements the new layout and design of the CHRT website, as designed by Redhead.


## Description 
This plugin implements a new design and layout for the CHRT website, while building on the existing functionality of the site.

## Changelog

### 0.3
* Updated color scheme and fonts to match brand refresh