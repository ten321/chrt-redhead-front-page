<?php
/*
Plugin Name: CHRT Front Page
Plugin URI:  http://chrt.org/
Description: Implements a separate design for the front page of the CHRT website
Version:     0.2
Author:      Ten-321 Enterprises
Author URI:  http://ten-321.com/
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die();
}

if ( ! class_exists( 'CHRT_Front_Page' ) ) {
	require_once( plugin_dir_path( __FILE__ ) . '/classes/class-chrt-front-page.php' );
}

global $chrt_front_page;
$chrt_front_page = CHRT_Front_Page::instance();