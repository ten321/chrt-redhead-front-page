;
/**
 * CHRT Front Page scripts
 */

jQuery( function( $ ) {
	$( 'body' ).removeClass( 'nojs' );
	
	if ( ! Modernizr.flexbox && ! Modernizr.flexboxlegacy && ! Modernizr.flexbox ) {
		$( '.front-page-feature' ).css( 'height', $( '.front-page-feature-wrap' ).outerHeight() + 'px' );
		$( document, 'window' ).on( 'resize', function() {
			$( '.front-page-feature' ).css( 'height', $( '.front-page-feature-wrap' ).outerHeight() + 'px' );
		} );
	}
	
	function CHRTCheckSearchVal() {
		if ( $( '.gsc-control-searchbox-only td.gsc-input td.gsib_a input' ).val() == '' ) {
			$( '.gsc-control-searchbox-only td.gsc-input td.gsib_a' ).removeClass( 'has-text' );
		} else {
			$( '.gsc-control-searchbox-only td.gsc-input td.gsib_a' ).addClass( 'has-text' );
		}
		return true;
	}
	
	$( '.widget_search' ).on( 'change keyup keydown focus blur', '.gsc-control-searchbox-only td.gsc-input td.gsib_a input', function() {
		return CHRTCheckSearchVal();
	} );
	
	$( '.widget_search' ).on( 'click', 'a.gsst_a', function() {
		return CHRTCheckSearchVal();
	} );
	
	$( '.front-below-content .widget:nth-of-type(1), .entry-content .buttons-row a:nth-of-type(4n+1)' ).addClass( 'first-of-type' );
	$( '.front-below-content .widget:nth-of-type(2), .entry-content .buttons-row a:nth-of-type(4n+2)' ).addClass( 'second-of-type' );
	$( '.front-below-content .widget:nth-of-type(3), .entry-content .buttons-row a:nth-of-type(4n+3)' ).addClass( 'third-of-type' );
	$( '.front-below-content .widget:nth-of-type(4), .entry-content .buttons-row a:nth-of-type(4n+4)' ).addClass( 'fourth-of-type' );
	
	/**
	 * Wrap the whole Featured Page widgets in the permalink
	 */
	$( '.front-below-content .featuredpage .widget-wrap' ).each( function() {
		var tmpLink = $( this ).find( 'a:first' );
		var tmpHref = tmpLink.attr( 'href' );
		tmpLink.replaceWith( tmpLink.html() );
		$( this ).wrap( '<a href="' + tmpHref + '"/>' );
	} );
	
	$( '.sidebar-primary .menu-item a:after' ).on( 'click', function() {
		$( this ).closest( 'a' ).addClass( 'hover' );
		return false;
	}, function() {
		$( this ).closest( 'a' ).removeClass( 'hover' );
		return false;
	} );
	
	$( '.sidebar-primary .menu-item-has-children > a' ).each( function() {
		$( this ).append( '<span class="expand-collapse"></span>' );
		
		$( this ).find( '.expand-collapse' ).on( 'click', function() {
			var $menu = $( this ).closest( '.menu-item-has-children' );
			
			if ( $menu.hasClass( 'current-menu-item' ) || $menu.hasClass( 'current-menu-ancestor' ) ) {
				return false;
			}
			
			if ( $menu.hasClass( 'hover' ) ) {
				$menu.removeClass( 'hover', 250 );
			} else {
				$( '.menu-item-has-children.hover' ).removeClass( 'hover' );
				$menu.addClass( 'hover', 250 );
			}
			return false;
		} );
	} );
	
	/**
	 * Add hoverIntent to sidebar navigation items
	 */
	/*$( '.sidebar-primary .menu-item' ).hover( function() {
		$(this).doTimeout( 'hover', 250, 'addClass', 'hover' );
	}, function() {
		$(this).doTimeout( 'hover', 250, 'removeClass', 'hover' );
	} );*/
		
	/**
	 * Set up the hamburger menu
	 */
	$( '.site-container .sidebar-primary' ).prepend( '<header id="mobile-header"><aside class="left"/><aside class="right"><a href="#" class="hamburger">Menu</a></aside></header>' );
	$( '.site-container .sidebar-primary' ).append( '<div id="hamburger-menu"><a href="#" class="hamburger-close">Close</a>' );
	$( '#hamburger-menu' ).append( $( '.front-top-sidebar' ).html() );
	$( '#hamburger-menu' ).append( $( '.front-bottom-sidebar' ).html() );
	$( '#hamburger-menu .site-logo' ).prependTo( $( '#mobile-header .left' ) );
	
	$( '#hamburger-menu .menu-item-has-children > a' ).each( function() {
		var tmpMenuItem = $( this ).clone();
		$( this ).attr( 'href', '#' );
		
		$( this ).closest( '.menu-item-has-children' ).find( '.sub-menu' ).prepend( tmpMenuItem );
		tmpMenuItem.wrap( '<li class="menu-item sub-parent added"/>' );
	} );
	
	$( '#hamburger-menu .menu-item-has-children > a' ).on( 'click', function() {
		var $menu = $( this ).closest( '.menu-item-has-children' );
		if ( $menu.hasClass( 'hover' ) ) {
			$menu.removeClass( 'hover', 250 );
		} else {
			$( '.menu-item-has-children.hover' ).removeClass( 'hover' );
			$menu.addClass( 'hover', 250 );
		}
		return false;
	} );
	
	$( '#hamburger-menu' ).hide();
	$( '#mobile-header .hamburger, .hamburger-close' ).on( 'click', function() {
		if ( $( '#hamburger-menu' ).hasClass( 'open' ) ) {
			$( '#hamburger-menu' ).removeClass( 'open', 500, 'swing', function() { $( this ).hide(); } );
		} else {
			$( '#hamburger-menu' ).show().addClass( 'open', 500, 'swing' );
		}
		return false;
	} );
	
	/**
	 * Allow a user to click anywhere in the body to close 
	 * 		the hamburger menu
	 */
	$( 'body' ).on( 'click', function() {
		$( '#hamburger-menu' ).toggleClass( 'open', 500, 'swing' );
	} ).find( '#hamburger-menu' ).on( 'click', function( e ) {
		e.stopPropagation();
	} );
	
	/**
	 * Allow the user to press the Esc key to close the 
	 * 		hamburger menu
	 */
	document.onkeydown = function(evt) {
		if ( ! $( '#hamburger-menu' ).hasClass( 'open' ) ) {
			return;
		}
		
		evt = evt || window.event;
		var isEscape = false;
		if ( "key" in evt ) {
			isEscape = evt.key == "Escape";
		} else {
			isEscape = evt.keyCode == 27;
		}
		if ( isEscape ) {
			$( '#hamburger-menu' ).toggleClass( 'open', 500, 'swing' );
		}
	};
	
	if ( $( 'body' ).hasClass( 'thumbs-gallery' ) ) {
		switchToThumbs();
	}
	
	function retrieveThumbs() {
		$( '.publications-entry' ).each( function() {
			retrieveThumb( this );
		} );
	}
	
	function retrieveThumb( e ) {
		var pubTitle = $( e ).find( '.publication-list-entry-title a' ).text();
		var pubThumb = $( e ).find( '.publication-list-feature' ).attr( 'data-thumb' );
		var pubLink = $( e ).find( '.publication-list-entry-title a' ).attr( 'href' );
		
		$( e ).find( 'header' ).append( '<figure class="publication-thumb" style="background-image: url(' + pubThumb + ')"><a href="' + pubLink + '"><span class="publication-title">' + pubTitle + '</span></a></figure>' );
		$( e ).find( '.publication-thumb' ).hide();
	}
	
	function switchToThumbs() {
		$( '.publications-entry' ).each( function() {
			if ( $( this ).find( '.publication-thumb' ).length <= 0 ) {
				retrieveThumb( this );
			}
		} );
		$( '.publication-thumb' ).show();
	}
	
	function switchToGrid() {
		$( '.publication-thumb' ).hide();
	}
	
	if ( document.querySelectorAll( '.gallery-terms' ).length >= 1 ) {
		/*$( '.gallery-wrapper' ).before( '<a href="#thumbnails" id="thumbs-link"><span class="view-switch dashicons dashicons-grid-view"></span> View as Grid</a>' );*/
		$( '.gallery-wrapper' ).before( '<a href="#" id="thumbs-link">Top of Gallery</a>' );
		$( '.content' ).append( '<a href="#thumbs-link" title="Return to top" id="list-top-link"><span class="dashicons dashicons-arrow-up-alt2"></span></a>' );
		$( '#list-top-link' ).on( 'click', function( e ) {
			e.preventDefault();
			$( 'html, body' ).animate({
				scrollTop: $( '#thumbs-link' ).offset().top
			}, 500);
		} );
		/*$( '#thumbs-link' ).on( 'click', function() {
			if ( $( 'body' ).hasClass( 'thumbs-gallery' ) ) {
				$( 'body' ).removeClass( 'thumbs-gallery' );
				switchToGrid();
				$( this ).find( '.view-switch' ).removeClass( 'dashicons-list-view' ).addClass( 'dashicons-grid-view' );
				$( this ).html( $( this ).html().replace( 'as List', 'as Grid' ) );
			} else {
				$( 'body' ).addClass( 'thumbs-gallery' );
				switchToThumbs();
				$( this ).find( '.view-switch' ).removeClass( 'dashicons-grid-view' ).addClass( 'dashicons-list-view' );
				$( this ).html( $( this ).html().replace( 'as Grid', 'as List' ) );
			}
			return false;
		} );*/
		/*$( '.content' ).wrapInner( '<div class="gallery-wrapper"/>' );
		$( '.gallery-wrapper .page-header' ).insertBefore( '.gallery-wrapper' );
		$( '.gallery-wrapper .archive-pagination' ).insertAfter( '.gallery-wrapper' );
		$( '.gallery-wrapper .archive-pagination' ).hide();*/
		$( '.gallery-terms' ).remove();
		$( '.gallery-wrapper' ).jscroll( { 
			pagingSelector : '.archive-pagination', 
			nextSelector: '.archive-pagination .pagination-next a', 
			contentSelector: '.content .gallery-wrapper', 
			callback : function( e ) { 
				if ( $( 'body' ).hasClass( 'thumbs-gallery' ) ) {
					switchToThumbs();
				}
			},
			debug : false, 
			loadingHtml : '<div class="jscroll-loading-wrap"><span class="jscroll-loading button">Loading&hellip;</span></div>'
		} );
	}
} );

if ( typeof( CHRTJS ) !== 'undefined' ) {
	CHRTJS.isTablet = function() {
		if ( jQuery( '.mobile-header' ).is( ':visible' ) ) {
			return true;
		}
		return false;
	};
	CHRTJS.isMobile = function() {
		return CHRTJS.isTablet();
	}
}