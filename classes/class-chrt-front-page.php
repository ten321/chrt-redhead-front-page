<?php
/**
 * Custom template for the new front page of the CHRT website
 * @version 0.1
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die();
}

if ( ! class_exists( 'CHRT_Front_Page' ) ) {
	/**
	 * The main class that implements the custom template
	 * @version 0.1
	 */
	class CHRT_Front_Page {
		/**
		 * Holds the version number for use with various assets
		 *
		 * @since  0.1
		 * @access public
		 * @var    string
		 */
		public $version = '0.5.1.1';
		
		/**
		 * Holds the class instance.
		 *
		 * @since	0.1
		 * @access	private
		 * @var		CHRT_Front_Page
		 */
		private static $instance;
		
		/**
		 * Returns the instance of this class.
		 *
		 * @access  public
		 * @since   0.1
		 * @return	CHRT_Front_Page
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) ) {
				$className = __CLASS__;
				self::$instance = new $className;
			}
			return self::$instance;
		}
		
		/**
		 * Create the CHRT_Front_Page object
		 */
		function __construct() {
			add_action( 'admin_init', array( $this, 'admin_init' ) );
			if ( defined( 'GENESIS_SETTINGS_FIELD' ) )
				$this->settings_field = GENESIS_SETTINGS_FIELD;
			else
				$this->settings_field = 'genesis-settings';
				
			$this->pagehook = $GLOBALS['_genesis_theme_settings_pagehook'];
			
			add_filter( 'body_class', array( $this, 'add_body_class' ) );
			add_action( 'after_setup_theme', array( $this, 'register_nav_menus' ) );
			add_action( 'after_setup_theme', array( $this, '_get_options' ) );
			add_action( 'genesis_setup', array( $this, 'register_sidebars' ) );
			add_action( 'init', array( $this, 'init' ), 11 );
			
			if ( is_admin() )
				return;
			
			add_action( 'wp', array( $this, 'startup' ) );
			add_filter( 'template_include', array( $this, 'template_override' ) );
		}
		
		/**
		 * Override any template files included in the child/parent theme
		 */
		function template_override( $template=null ) {
			global $wp_query;
			if ( ! $wp_query->is_search ) {
				return $template;
			}
			
			return plugin_dir_path( dirname( __FILE__ ) ) . 'templates/search.php';
		}
		
		/**
		 * Perform any admin-specific actions that need to happen
		 */
		function admin_init() {
			add_action( 'genesis_theme_settings_metaboxes', array( $this, 'register_theme_settings_metabox' ) );
		}
		
		/**
		 * Register a custom meta box in the Genesis -> Theme Settings admin page
		 */
		function register_theme_settings_metabox() {
			add_meta_box( 'chrt-redhead-options', __( 'CHRT Options' ), array( $this, 'chrt_options_metabox' ), $this->pagehook, 'main', 'high' );
		}
		
		/**
		 * Output the theme-specific options
		 */
		function chrt_options_metabox() {
?>
<p><label for="<?php echo $this->get_field_id( '_main_tagline' ) ?>"><?php _e( 'What is the primary tagline for the site (i.e. what should show up immediately below the logo)?' ) ?></label> <br/>
	<input type="text" name="<?php echo $this->get_field_name( '_main_tagline' ) ?>" id="<?php echo $this->get_field_id( '_main_tagline' ) ?>" value="<?php echo esc_attr( $this->tagline['main'] ) ?>" class="widefat"/></p>
<p><label for="<?php echo $this->get_field_id( '_secondary_tagline' ) ?>"><?php _e( 'What is the secondary tagline for the site (i.e. what should show up below the primary tagline)?' ) ?></label> <br/>
	<input type="text" name="<?php echo $this->get_field_name( '_secondary_tagline' ) ?>" id="<?php echo $this->get_field_id( '_secondary_tagline' ) ?>" value="<?php echo esc_attr( $this->tagline['secondary'] ) ?>" class="widefat"/></p>
<?php
			
			$terms = get_terms( array(
				'taxonomy'   => 'bio-type', 
				'hide_empty' => false, 
			) );
			
			foreach ( $terms as $term ) {
?>
<p><label for="<?php echo $this->get_field_id( '_bio_type_page_id_' . $term->term_id ) ?>"><?php printf( __( 'Which page serves as the main archive for %s biographies?' ), $term->name ) ?></label> <br/>
	<?php wp_dropdown_pages( array( 
	'show_option_none' => __( '-- No Archive Page --' ), 
	'selected'         => $this->bio_type_page_id[$term->term_id], 
	'name'             => $this->get_field_name( '_bio_type_page_id' ) . '[' . $term->term_id . ']', 
	'id'               => $this->get_field_id( '_bio_type_page_id_' . $term->term_id ), 
	'class'            => 'widefat', 
	) ) ?></p>
<?php
			}
		}
		
		/**
		 * Retrieve an input name for the custom Genesis Settings
		 */
		function get_field_name( $id ) {
			return sprintf( '%s[%s]', $this->settings_field, $id );
		}
		
		/**
		 * Retrieve an input ID for the custom Genesis Settings
		 */
		function get_field_id( $id ) {
			return sprintf( '%s[%s]', $this->settings_field, $id );
		}
		
		/**
		 * Retrieve the value of a specific custom Genesis setting
		 */
		function get_field_value( $key ) {
			return genesis_get_option( $key, $this->settings_field );
		}
		
		/**
		 * Set up the default settings for each custom Genesis setting
		 */
		function default_settings( $settings ) {
			$settings['_bio_type_page_id'] = array();
			$settings['tagline'] = array( 
				'main'      => __( 'Center for Healthcare Research &amp; Transformation' ), 
				'secondary' => __( 'Improving Health. Informing Policy.' ) 
			);
			return $settings;
		}
		
		/**
		 * Retrieve the custom Genesis settings
		 */
		function _get_options() {
			$this->bio_type_page_id = genesis_get_option( '_bio_type_page_id' );
			$this->tagline = array( 
				'main'      => esc_attr( genesis_get_option( '_main_tagline' ) ), 
				'secondary' => esc_attr( genesis_get_option( '_secondary_tagline' ) ), 
			);
			
			add_image_size( 'redhead-feature', 900, 600, true );
			add_image_size( 'inner-feature', 900, 250, true );
			remove_image_size( 'publication-small' );
			add_image_size( 'publication-small', 175, 10000, false );
		}
		
		/**
		 * Add a custom body class
		 */
		function add_body_class( $classes=array() ) {
			$classes[] = 'front-page-2016';
			$classes[] = 'nojs';
			if ( is_search() ) {
				$classes[] = 'archive';
				add_filter( 'get_search_form', function( $var ) { global $chrt_theme_obj; $chrt_theme_obj->did_search_form = true; return $var; } );
			}
			return $classes;
		}
		
		/**
		 * Determine if this is the front page or not, and, if so, hook into the 
		 * 		actions we need to hook into
		 */
		function startup() {
			/*if ( ! is_front_page() )
				return;*/
			
			add_action( 'template_redirect', array( $this, 'template_redirect' ), 99 );
		}
		
		/**
		 * Perform any changes that need to happen on init
		 */
		function init() {
            $this->register_taxonomies();
		}

		/**
		 * Register the Publication Tag taxonomy for Issue Briefs
         *
         * @access public
         * @since  2019.06
         * @return void
		 */
		function register_taxonomies() {

			/**
			 * Taxonomy: Publication Tags.
			 */

			$labels = array(
				"name" => __( "Publication Tags", "chrt-front-page" ),
				"singular_name" => __( "Publication Tag", "chrt-front-page" ),
			);

			$args = array(
				"label" => __( "Publication Tags", "news-pro" ),
				"labels" => $labels,
				"public" => true,
				"publicly_queryable" => true,
				"hierarchical" => false,
				"show_ui" => true,
				"show_in_menu" => true,
				"show_in_nav_menus" => true,
				"query_var" => true,
				"rewrite" => array( 'slug' => 'pub_tag', 'with_front' => false, ),
				"show_admin_column" => true,
				"show_in_rest" => true,
				"rest_base" => "pub_tag",
				"rest_controller_class" => "WP_REST_Terms_Controller",
				"show_in_quick_edit" => true,
			);
			register_taxonomy( "pub_tag", array( "publication" ), $args );

			$hct = get_taxonomy( 'trend' );
			$hct->show_admin_column = false;
			register_taxonomy( 'trend', 'publication', (array) $hct );
		}
		
		/**
		 * Set up two new menus specifically for the front page
		 */
		function register_nav_menus() {
			register_nav_menu( 'front-primary-nav', __( 'Primary Front Page Navigation' ) );
			register_nav_menu( 'front-trends-nav', __( 'Secondary Front Page Navigation' ) );
		}
		
		/**
		 * Set up any widgetized areas that apply to this template
		 */
		function register_sidebars() {
			genesis_register_sidebar( array(
				'id' => 'front-top-sidebar', 
				'name' => __( 'Top Sidebar' ), 
				'description' => __( 'Appears at the top of the primary sidebar.' )
			) );
			genesis_register_sidebar( array(
				'id' => 'front-bottom-sidebar', 
				'name' => __( 'Bottom Sidebar' ), 
				'description' => __( 'Appears at the bottom of the primary sidebar.' )
			) );
			genesis_register_sidebar( array(
				'id'          => 'front-feature',
				'name'        => __( '[Front Page] Featured News' ), 
				'description' => __( 'Appears between the main feature and the boxes on the front page; meant for featuring new posts and press releases' )
			) );
			genesis_register_sidebar( array(
				'id' => 'front-below-content', 
				'name' => __( '[Front Page] Below Content' ), 
				'description' => __( 'Appears below the main feature on the front page; each widget is one-quarter width on a desktop display' )
			) );
			genesis_register_sidebar( array(
				'id' => 'consulting-services', 
				'name' => __( '[Consulting Services] Secondary Sidebar' ), 
				'description' => __( 'Appears on the right side of all pages within the Consulting Services section of the website' )
			) );
			
			//* Unregister content/sidebar layout setting
			genesis_unregister_layout( 'content-sidebar' );
			 
			//* Unregister sidebar/content layout setting
			genesis_unregister_layout( 'sidebar-content' );
			 
			//* Unregister content/sidebar/sidebar layout setting
			genesis_unregister_layout( 'content-sidebar-sidebar' );
			 
			//* Unregister sidebar/sidebar/content layout setting
			genesis_unregister_layout( 'sidebar-sidebar-content' );
			 
			//* Unregister sidebar/content/sidebar layout setting
			genesis_unregister_layout( 'sidebar-content-sidebar' );
			 
			//* Unregister full-width content layout setting
			genesis_unregister_layout( 'full-width-content' );
			
			remove_theme_support( 'genesis-inpost-layouts' );
			
		}
		
		/**
		 * Check if this is the front page, and, if so, make all of the necessary
		 * 		adjustments
		 */
		function template_redirect() {
			/*if ( ! is_front_page() ) 
				return;*/
			
			/**
			 * Get rid of the original CHRT theme styles/scripts
			 */
			global $chrt_features_obj, $chrt_theme_obj;
			/*remove_action( 'wp_enqueue_scripts', array( $chrt_features_obj, 'enqueue_scripts' ) );*/
			/*remove_action( 'wp_enqueue_scripts', array( $chrt_features_obj, 'enqueue_styles' ) );*/
			remove_action( 'genesis_after_header', array( $chrt_theme_obj, 'swap_sidebars' ) );
			add_action( 'genesis_after_header', array( $this, 'fix_sidebars' ), 11 );
			remove_action( 'genesis_sidebar_alt', array( $chrt_theme_obj, 'do_related_nav' ), 1 );
			remove_action( 'genesis_sidebar', array( $chrt_theme_obj, 'do_related_nav' ), 1 );
			remove_action( 'template_redirect', array( $chrt_theme_obj, 'template_redirect' ) );
			
			// Force full width content layout
			add_filter( 'genesis_site_layout', function() { return 'sidebar-content'; } );
			
			add_action( 'wp_head', array( $this, 'adjust_genesis' ) );
			
			// Set up the necessary scripts and styles
			add_action( 'wp_enqueue_scripts', array( $this, 'add_scripts' ), 11 );
			add_action( 'wp_print_footer_scripts', array( $this, 'footer_scripts' ), 11 );
			add_action( 'wp_print_styles', array( $this, 'add_styles' ), 11 );
			
			if ( ! is_front_page() && is_singular() ) {
				add_action( 'genesis_before_entry', array( $this, 'insert_featured_image' ) );
			}
			
			add_filter( 'wpv_filter_query', array( $this, 'filter_view_query' ), 99, 3 );
			/*add_filter( 'wpv_view_settings', array( $this, 'filter_view_settings' ), 99, 2 );*/
			
			if ( is_singular( 'bio' ) ) {
				add_filter( 'nav_menu_css_class', array( $this, 'bio_type_parent_page' ), 10, 2 );
			}
			
			if ( is_page_template( 'template-policy-fellowship.php' ) ) {
				global $chrt_theme_obj;
				remove_action( 'genesis_sidebar', array( $chrt_theme_obj, '_do_fellowship_sidebar' ) );
				remove_action( 'genesis_before_content', array( $chrt_theme_obj, 'do_page_header' ) );
				add_action( 'genesis_before_entry', array( $this, 'fellowship_secondary_sidebar' ), 11 );
				add_action( 'genesis_entry_header', array( $chrt_theme_obj, 'do_page_header' ) );
			} else if ( $this->is_tree( 'consulting-services-overview' ) ) {
				add_action( 'genesis_before_entry', array( $this, 'consulting_secondary_sidebar' ), 11 );
				add_filter( 'body_class', function( $classes ) { $classes[] = 'consulting-services-template'; return $classes; } );
			} else if ( is_post_type_archive( 'publication' ) || is_tax( 'pub-category' ) ) {
				global $chrt_theme_obj;
				remove_filter( 'genesis_pre_get_option_site_layout', array( $chrt_theme_obj, 'publication_site_layout' ), 101 );
				/*remove_action( 'genesis_before_loop', array( $chrt_theme_obj, 'publication_header' ) );
				add_action( 'genesis_entry_header', array( $chrt_theme_obj, 'ajax_gallery_before_loop' ) );*/
				
				if ( isset( $_GET['layout'] ) && 'thumbs' == $_GET['layout'] ) {
					add_filter( 'body_class', function( $classes ) { $classes[] = 'thumbs-gallery'; return $classes; } );
				}
				
				add_action( 'genesis_before_loop', function() { echo '<div class="gallery-wrapper">'; }, 99 );
				add_action( 'genesis_after_loop', function() { echo '</div>'; }, 1 );
				
				$vals = get_option( 'genesis-cpt-archive-settings-publication', array() );
				if ( is_array( $vals ) && array_key_exists( 'intro_text', $vals ) ) {
					add_action( 'genesis_before_loop', array( $this, 'publications_intro_text' ) );
				}
			} else if ( is_post_type_archive( 'news' ) || is_tax( 'news-category' ) ) {
				global $chrt_theme_obj;
				remove_all_filters( 'genesis_pre_get_option_site_layout' );
				remove_all_actions( 'genesis_before_loop' );
				add_action( 'genesis_before_loop', array( $this, 'news_archive_header' ) );
				remove_action( 'genesis_sidebar', array( $chrt_theme_obj, 'archive_sidebar' ) );
				remove_action( 'genesis_before_loop', array( $chrt_theme_obj, 'archive_before_loop' ) );
				
				/*remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
				remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
				add_filter( 'genesis_post_info', array( $this, 'archive_post_info' ) );
				remove_all_actions( 'genesis_sidebar' );
				remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
				remove_action( 'genesis_after_post_content', 'genesis_post_meta' );*/
			} else if ( is_home() || is_category() || is_tag() ) {
				add_action( 'genesis_before_loop', array( $this, 'blog_archive_header' ) );
			} else if ( is_search() ) {
				add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_sidebar_content' );
				remove_all_actions( 'genesis_loop' ); 
				add_action( 'genesis_before_loop', array( $this, 'search_header' ) );
				add_action( 'genesis_loop', array( $this, 'do_search_results' ) );
			}
		}
		
		function blog_archive_header() {
			$title = single_post_title( '', false );
?>
<header class="page-header">
	<h1 class="page-title"><?php echo $title ?></h1>
</header>
<?php
		}
		
		/**
		 * Output a special header just for the search results page
		 */
		function search_header() {
			$title = apply_filters( 'chrt-search-title', __( 'Search Results', 'chrt' ) );
?>
<header class="page-header">
	<h1 class="page-title"><?php echo $title ?></h1>
    <?php do_action( 'chrt-search-header' ) ?>
</header>
<?php
		}
		
		/**
		 * Output the Google search results body
		 */
		function do_search_results() {
			if ( isset( $GLOBALS['chrt_theme_obj'] ) && true !== $GLOBALS['chrt_theme_obj']->did_search_form ) {
?>
<script>
  (function() {
	var cx = '014095031900730959029:pst5q3j7idk';
	var gcse = document.createElement('script');
	gcse.type = 'text/javascript';
	gcse.async = true;
	gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
		'//cse.google.com/cse.js?cx=' + cx;
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(gcse, s);
  })();
</script>
<?php
			}
?>
<gcse:searchresults-only></gcse:searchresults-only>
<?php
		}
		
		/**
		 * Output some introductory text before the publication archive body
		 */
		function publications_intro_text() {
			$vals = get_option( 'genesis-cpt-archive-settings-publication', array() );
			if ( ! is_array( $vals ) || ! array_key_exists( 'intro_text', $vals ) )
				return;
			
			printf( '<div class="archive-intro">%s</div>', apply_filters( 'the_content', $vals['intro_text'] ) );
		}
		
		/**
		 * Output a custom title/header at the top of News pages
		 */
		function news_archive_header() {
			$title = __( 'In The Media' );
			
			if ( is_tax() ) {
				$ob = get_queried_object();
				if ( property_exists( $ob, 'parent' ) && ! empty( $ob->parent ) && property_exists( $ob, 'name' ) ) {
					$parent = clone $ob;
					while ( ! empty( $parent->parent ) ) {
						$parent = get_term( $ob->parent, $ob->taxonomy );
					}
					$title = $parent->name . ': ' . $ob->name;
				} else if ( property_exists( $ob, 'name' ) ) {
					$title = $ob->name;
				}
			}
?>
<header class="page-header">
	<h1 class="page-title"><?php echo $title ?></h1>
<?php
			global $chrt_theme_obj;
			$chrt_theme_obj->news_intro_text();
?>
</header>
<?php
		}
		
		function fellowship_secondary_sidebar() {
			echo '<aside class="fellowship-sidebar">';
			global $chrt_theme_obj;
			$chrt_theme_obj->_do_fellowship_sidebar();
			echo '</aside>';
		}
		
		function consulting_secondary_sidebar() {
			echo '<aside class="consulting-sidebar">';
			$this->_do_consulting_sidebar();
			echo '</aside>';
		}
		
		function _do_consulting_sidebar() {
			if ( ! is_active_sidebar( 'consulting-services' ) ) {
				return;
			}
			
			dynamic_sidebar( 'consulting-services' );
		}
		
		/**
		 * Adjust the custom menu to recognize a specific bio is the child of the archive page
		 */
		function bio_type_parent_page( $classes, $item ) {
			if ( ! is_singular( 'bio' ) )
				return $classes;
			
			$bio = get_queried_object();
			$types = get_the_terms( $bio, 'bio-type' );
			
			if ( false === $types || is_wp_error( $types ) )
				return $classes;
			
			$type = array_shift( $types );
			if ( ! array_key_exists( $type->term_id, $this->bio_type_page_id ) || empty( $this->bio_type_page_id[$type->term_id] ) )
				return $classes;
			
			$parent_page = $this->bio_type_page_id[$type->term_id];
			$parent_page = get_post( $parent_page );
			
			if ( $item->object_id == $parent_page->ID ) {
				$classes[] = 'current-menu-item';
			}
			
			$ancestors = get_ancestors( $parent_page->ID, $parent_page->post_type );
			if ( ! is_array( $ancestors ) )
				return $classes;
			if ( ! in_array( $item->object_id, $ancestors ) )
				return $classes;
			
			$classes[] = 'current-page-ancestor';
			$classes[] = 'current-menu-ancestor';
			
			return $classes;
		}
		
		/**
		 * Adjust a specific View's settings before it is rendered
		 */
		function filter_view_settings( $settings, $id ) {
			if ( 5472 != intval( $id ) )
				return $settings;
			
			$settings['orderby'] = array( 'sticky_clause' => 'DESC', 'last_name_clause' => 'ASC' );
			return $settings;
		}
		
		/**
		 * Add any necessary arguments to View queries
		 * @param array $args the WP query args
		 * @param array $settings the View settings
		 * @param array $id the View ID
		 */
		function filter_view_query( $args, $settings, $id ) {
			if ( 5472 != intval( $id ) )
				return $args;
			
			$args['orderby'] = array( 'sticky_clause' => 'DESC', 'last_name_clause' => 'ASC' );
			$args['meta_query'] = array(
				'relation' => 'AND', 
				'sticky_clause' => array(
					'key' => 'wpcf-sticky', 
					'type' => 'NUMBER', 
				), 
				'last_name_clause' => array(
					'key' => 'wpcf-ordinal-name', 
				), 
			);
			
			return $args;
		}
		
		/**
		 * Insert the featured image at the top of the page, if there is one
		 */
		function insert_featured_image() {
			if ( is_singular( 'chrt-event' ) ) {
				return;
			}
			
			if ( is_page_template( 'template-policy-fellowship.php' ) ) {
				global $post;
				if ( ! empty( $post->post_parent ) ) {
					$parents = get_ancestors( $post->ID, $post->post_type );
					$top = array_pop( $parents );
				} else {
					$top = $post->ID;
				}
				
				if ( ! has_post_thumbnail( $top ) ) {
					return;
				}
				
			} else {
				
				if ( ! has_post_thumbnail() ) 
					return;
				if ( is_singular( 'bio' ) || is_singular( 'publication' ) )
					return;
				
				$top = get_the_ID();
			}
			
			$id = get_post_thumbnail_id( $top );
			
			if ( empty( $id ) ) {
				return;
			}
   
			$img = wp_get_attachment_image_src( $id, 'inner-feature', false );
			
			if ( $img[1] < 900 ) {
				if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
					print( "\n<!-- The image dimensions reported smaller than the minimum. Those dimensions were: \n" );
					print_r( $img );
					print( "\n-->" );
				}
				/*return;*/
			}
			
			printf( '<figure class="chrt-inner-feature">%s</figure>', get_the_post_thumbnail( $top, 'inner-feature' ) );
		}
		
		function fix_sidebars() {
		}
		
		function adjust_genesis() {
			// Remove site header elements
			remove_all_actions( 'genesis_header' );
			
			// Remove breadcrumbs
			remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );
			
			// Remove footer elements
			remove_all_actions( 'genesis_before_footer' );
			remove_all_actions( 'genesis_footer' );
			
			if ( is_front_page() ) {
				// Remove the standard loop and replace it with this page template
				remove_all_actions( 'genesis_loop' );
				add_action( 'genesis_loop', array( $this, 'do_front_page_loop' ) );
			}
			
			remove_all_actions( 'genesis_sidebar' );
			add_action( 'genesis_sidebar', array( $this, 'do_front_page_sidebar' ) );
			
			if ( is_singular( 'bio' ) ) {
				remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
			}
		}
		
		/**
		 * Add necessary style sheets
		 */
		function add_styles() {
			if ( wp_style_is( 'chrt', 'enqueued' ) ) {
				wp_dequeue_style( 'chrt' );
			}
			if ( ! wp_style_is( 'genesis', 'registered' ) ) {
				wp_register_style( 'genesis', get_stylesheet_uri(), array(), $this->version, 'all' );
			}
			wp_register_style( 'sentinel-font', 'https://cloud.typography.com/6622056/7992592/css/fonts.css', array(), $this->version, 'all' );
			wp_register_style( 'chrt-redhead', plugins_url( '/styles/chrt-2018.css', dirname( __FILE__ ) ), array( 'genesis', 'dashicons', 'sentinel-font' ), $this->version, 'all' );
			if ( is_front_page() ) {
				wp_enqueue_style( 'chrt-front-page', plugins_url( '/styles/chrt-front-page-2018.css', dirname( __FILE__ ) ), array( 'chrt-redhead' ), $this->version, 'all' );
			} else {
				wp_enqueue_style( 'chrt-redhead' );
			}

			add_action( 'wp_footer', function() {
			    printf( '<svg class="defs-only">
<filter id="imgoverlay" 
    color-interpolation-filters="sRGB"
    x="0" y="0" height="100%%" width="100%%">
  <feColorMatrix type="matrix"
   values="%1$s 0 0 0 0 
           %2$s 0 0 0 0 
           %3$s 0 0 0 0 
           0    0 0 1 0" />
</filter>
<filter id="whiteoverlay" 
    color-interpolation-filters="sRGB"
    x="0" y="0" height="100%%" width="100%%">
  <feColorMatrix type="matrix"
   values="%4$d 0 0 0 0 
           %4$d 0 0 0 0 
           %4$d 0 0 0 0 
           0    0 0 1 0" />
</filter>
</svg>', '.99', '.81', '.61', 1 );
            } );
		}
		
		/**
		 * Add necessary header scripts
		 */
		function add_scripts() {
			wp_register_script( 'dotimeout', plugins_url( '/scripts/jquery.ba-dotimeout.min.js', dirname( __FILE__ ) ), array( 'jquery' ), $this->version, true );
			/*wp_register_script( 'burgermenu', plugins_url( '/scripts/jquery.ultimate-burger-menu.js', dirname( __FILE__ ) ), array( 'jquery' ), $this->version, true );*/
			$deps = array( 'jquery-effects-core', 'dotimeout' );
			if ( is_post_type_archive( 'publication' ) || is_tax( 'pub-category' ) ) {
				wp_register_script( 'jscroll', plugins_url( '/scripts/jquery.jscroll.min.js', dirname( __FILE__ ) ), array( 'jquery' ), $this->version, true );
				$deps[] = 'jscroll';
			}
			
			if ( wp_script_is( 'chrt-front-page', 'registered' ) ) {
				wp_deregister_script( 'chrt-front-page' );
			}
			wp_enqueue_script( 'chrt-front-page', plugins_url( '/scripts/chrt-front-page.js', dirname( __FILE__ ) ), $deps, $this->version, true );
			
			wp_dequeue_script( 'chrt-accordion' );
			wp_dequeue_script( 'chrt-gallery' );
			wp_dequeue_script( 'chrt-header' );
			return;
		}
		
		/**
		 * Add necessary footer scripts
		 */
		function footer_scripts() {
?>
<script type="text/javascript">
    /*! modernizr 3.6.0 (Custom Build) | MIT *
	 * https://modernizr.com/download/?-cssfilters-flexbox-flexboxlegacy-flexboxtweener-rgba-svgasimg-svgfilters-setclasses !*/
    !function(e,n,t){function r(e,n){return typeof e===n}function o(){var e,n,t,o,s,i,l;for(var a in C)if(C.hasOwnProperty(a)){if(e=[],n=C[a],n.name&&(e.push(n.name.toLowerCase()),n.options&&n.options.aliases&&n.options.aliases.length))for(t=0;t<n.options.aliases.length;t++)e.push(n.options.aliases[t].toLowerCase());for(o=r(n.fn,"function")?n.fn():n.fn,s=0;s<e.length;s++)i=e[s],l=i.split("."),1===l.length?Modernizr[l[0]]=o:(!Modernizr[l[0]]||Modernizr[l[0]]instanceof Boolean||(Modernizr[l[0]]=new Boolean(Modernizr[l[0]])),Modernizr[l[0]][l[1]]=o),x.push((o?"":"no-")+l.join("-"))}}function s(e){var n=w.className,t=Modernizr._config.classPrefix||"";if(b&&(n=n.baseVal),Modernizr._config.enableJSClass){var r=new RegExp("(^|\\s)"+t+"no-js(\\s|$)");n=n.replace(r,"$1"+t+"js$2")}Modernizr._config.enableClasses&&(n+=" "+t+e.join(" "+t),b?w.className.baseVal=n:w.className=n)}function i(){return"function"!=typeof n.createElement?n.createElement(arguments[0]):b?n.createElementNS.call(n,"http://www.w3.org/2000/svg",arguments[0]):n.createElement.apply(n,arguments)}function l(e,n){return!!~(""+e).indexOf(n)}function a(e){return e.replace(/([a-z])-([a-z])/g,function(e,n,t){return n+t.toUpperCase()}).replace(/^-/,"")}function u(e,n){return function(){return e.apply(n,arguments)}}function f(e,n,t){var o;for(var s in e)if(e[s]in n)return t===!1?e[s]:(o=n[e[s]],r(o,"function")?u(o,t||n):o);return!1}function c(e){return e.replace(/([A-Z])/g,function(e,n){return"-"+n.toLowerCase()}).replace(/^ms-/,"-ms-")}function d(n,t,r){var o;if("getComputedStyle"in e){o=getComputedStyle.call(e,n,t);var s=e.console;if(null!==o)r&&(o=o.getPropertyValue(r));else if(s){var i=s.error?"error":"log";s[i].call(s,"getComputedStyle returning null, its possible modernizr test results are inaccurate")}}else o=!t&&n.currentStyle&&n.currentStyle[r];return o}function p(){var e=n.body;return e||(e=i(b?"svg":"body"),e.fake=!0),e}function m(e,t,r,o){var s,l,a,u,f="modernizr",c=i("div"),d=p();if(parseInt(r,10))for(;r--;)a=i("div"),a.id=o?o[r]:f+(r+1),c.appendChild(a);return s=i("style"),s.type="text/css",s.id="s"+f,(d.fake?d:c).appendChild(s),d.appendChild(c),s.styleSheet?s.styleSheet.cssText=e:s.appendChild(n.createTextNode(e)),c.id=f,d.fake&&(d.style.background="",d.style.overflow="hidden",u=w.style.overflow,w.style.overflow="hidden",w.appendChild(d)),l=t(c,e),d.fake?(d.parentNode.removeChild(d),w.style.overflow=u,w.offsetHeight):c.parentNode.removeChild(c),!!l}function g(n,r){var o=n.length;if("CSS"in e&&"supports"in e.CSS){for(;o--;)if(e.CSS.supports(c(n[o]),r))return!0;return!1}if("CSSSupportsRule"in e){for(var s=[];o--;)s.push("("+c(n[o])+":"+r+")");return s=s.join(" or "),m("@supports ("+s+") { #modernizr { position: absolute; } }",function(e){return"absolute"==d(e,null,"position")})}return t}function h(e,n,o,s){function u(){c&&(delete N.style,delete N.modElem)}if(s=r(s,"undefined")?!1:s,!r(o,"undefined")){var f=g(e,o);if(!r(f,"undefined"))return f}for(var c,d,p,m,h,v=["modernizr","tspan","samp"];!N.style&&v.length;)c=!0,N.modElem=i(v.shift()),N.style=N.modElem.style;for(p=e.length,d=0;p>d;d++)if(m=e[d],h=N.style[m],l(m,"-")&&(m=a(m)),N.style[m]!==t){if(s||r(o,"undefined"))return u(),"pfx"==n?m:!0;try{N.style[m]=o}catch(y){}if(N.style[m]!=h)return u(),"pfx"==n?m:!0}return u(),!1}function v(e,n,t,o,s){var i=e.charAt(0).toUpperCase()+e.slice(1),l=(e+" "+j.join(i+" ")+i).split(" ");return r(n,"string")||r(n,"undefined")?h(l,n,o,s):(l=(e+" "+A.join(i+" ")+i).split(" "),f(l,n,t))}function y(e,n,r){return v(e,t,t,n,r)}function _(e,n){if("object"==typeof e)for(var t in e)O(e,t)&&_(t,e[t]);else{e=e.toLowerCase();var r=e.split("."),o=Modernizr[r[0]];if(2==r.length&&(o=o[r[1]]),"undefined"!=typeof o)return Modernizr;n="function"==typeof n?n():n,1==r.length?Modernizr[r[0]]=n:(!Modernizr[r[0]]||Modernizr[r[0]]instanceof Boolean||(Modernizr[r[0]]=new Boolean(Modernizr[r[0]])),Modernizr[r[0]][r[1]]=n),s([(n&&0!=n?"":"no-")+r.join("-")]),Modernizr._trigger(e,n)}return Modernizr}var x=[],C=[],S={_version:"3.6.0",_config:{classPrefix:"",enableClasses:!0,enableJSClass:!0,usePrefixes:!0},_q:[],on:function(e,n){var t=this;setTimeout(function(){n(t[e])},0)},addTest:function(e,n,t){C.push({name:e,fn:n,options:t})},addAsyncTest:function(e){C.push({name:null,fn:e})}},Modernizr=function(){};Modernizr.prototype=S,Modernizr=new Modernizr,Modernizr.addTest("svgfilters",function(){var n=!1;try{n="SVGFEColorMatrixElement"in e&&2==SVGFEColorMatrixElement.SVG_FECOLORMATRIX_TYPE_SATURATE}catch(t){}return n});var w=n.documentElement,b="svg"===w.nodeName.toLowerCase();Modernizr.addTest("rgba",function(){var e=i("a").style;return e.cssText="background-color:rgba(150,255,150,.5)",(""+e.backgroundColor).indexOf("rgba")>-1});var T=S._config.usePrefixes?" -webkit- -moz- -o- -ms- ".split(" "):["",""];S._prefixes=T;var E="CSS"in e&&"supports"in e.CSS,P="supportsCSS"in e;Modernizr.addTest("supports",E||P);var z="Moz O ms Webkit",j=S._config.usePrefixes?z.split(" "):[];S._cssomPrefixes=j;var A=S._config.usePrefixes?z.toLowerCase().split(" "):[];S._domPrefixes=A;var k={elem:i("modernizr")};Modernizr._q.push(function(){delete k.elem});var N={style:k.elem.style};Modernizr._q.unshift(function(){delete N.style}),S.testAllProps=v,S.testAllProps=y,Modernizr.addTest("cssfilters",function(){if(Modernizr.supports)return y("filter","blur(2px)");var e=i("a");return e.style.cssText=T.join("filter:blur(2px); "),!!e.style.length&&(n.documentMode===t||n.documentMode>9)}),Modernizr.addTest("flexbox",y("flexBasis","1px",!0)),Modernizr.addTest("flexboxlegacy",y("boxDirection","reverse",!0)),Modernizr.addTest("flexboxtweener",y("flexAlign","end",!0));var O;!function(){var e={}.hasOwnProperty;O=r(e,"undefined")||r(e.call,"undefined")?function(e,n){return n in e&&r(e.constructor.prototype[n],"undefined")}:function(n,t){return e.call(n,t)}}(),S._l={},S.on=function(e,n){this._l[e]||(this._l[e]=[]),this._l[e].push(n),Modernizr.hasOwnProperty(e)&&setTimeout(function(){Modernizr._trigger(e,Modernizr[e])},0)},S._trigger=function(e,n){if(this._l[e]){var t=this._l[e];setTimeout(function(){var e,r;for(e=0;e<t.length;e++)(r=t[e])(n)},0),delete this._l[e]}},Modernizr._q.push(function(){S.addTest=_}),Modernizr.addTest("svgasimg",n.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image","1.1")),o(),s(x),delete S.addTest,delete S.addAsyncTest;for(var V=0;V<Modernizr._q.length;V++)Modernizr._q[V]();e.Modernizr=Modernizr}(window,document);
</script>
<?php
			return;
		}
		
		/**
		 * Output the main sidebar on the front page
		 */
		function do_front_page_sidebar() {
			echo '<aside class="front-top-sidebar">';
			printf( '<figure class="site-logo">
    <a href="/">
        <svg width="160" height="84.4531">
            <image xlink:href="%s" src="%s" alt="CHRT" width="160" height="84.4531"></image>
        </svg>
        <figcaption>%s</figcaption>
    </a>
</figure>',
                plugins_url( '/assets/images/chrt-color-logo.svg', dirname( __FILE__ ) ),
                plugins_url( '/assets/images/chrt-color-logo.png', dirname( __FILE__ ) ),
                $this->tagline['main'] . '<span class="sub-tagline">' . html_entity_decode( $this->tagline['secondary'] ) . '</span>'
            );
			if ( is_active_sidebar( 'front-top-sidebar' ) ) {
				dynamic_sidebar( 'front-top-sidebar' );
			} else {
				printf( '<div class="search-form-wrap">%s</div>', get_search_form( false ) );
				wp_nav_menu( array( 'theme_location' => 'front-primary-nav', 'container' => 'nav' ) );
			}
			echo '</aside><aside class="front-bottom-sidebar">';
			if ( is_active_sidebar( 'front-bottom-sidebar' ) ) {
				dynamic_sidebar( 'front-bottom-sidebar' );
			} else {
				wp_nav_menu( array( 'theme_location' => 'front-trends-nav', 'container' => 'nav' ) );
			}
			echo '</aside>';
		}
		
		/**
		 * Output the main content area of the front page
		 */
		function do_front_page_loop() {
			if ( have_posts() ) : while ( have_posts() ) : the_post();
				$img = get_post_thumbnail_id( get_the_ID() );
				$imginfo = wp_get_attachment_image_src( $img, 'redhead-feature' );
				printf( '<div class="front-page-feature" style="background-image: url(%1$s)"><img class="ie-front-page-feature" src="%1$s"/><div class="front-page-feature-wrap">%2$s</div></div>', $imginfo[0], sprintf( '<div class="front-page-caption"><header><h1 class="entry-title">%s</h1></header><div class="feature-content">%s</div></div>', apply_filters( 'the_title', get_the_title() ), apply_filters( 'the_content', get_the_content() ) ) );
			endwhile; endif;
			
			if ( is_active_sidebar( 'front-feature' ) ) {
				echo '<aside class="front-feature">';
				dynamic_sidebar( 'front-feature' );
				echo '</aside>';
			}
			
			if ( is_active_sidebar( 'front-below-content' ) ) {
				remove_filter( 'the_content', 'sharing_display', 19 );
				echo '<aside class="front-below-content">';
				dynamic_sidebar( 'front-below-content' );
				echo '</aside>';
				add_filter( 'the_content', 'sharing_display', 19 );
			}
		}
		
		/**
		 * Check to see if this is a specific page, or a child of that specific page
		 * @param   mixed $pid the ID, slug or title of the page we're looking for
		 *
		 * @since   1.0.2
		 * @access  public
		 * @return  bool
		 */
		function is_tree( $pid ) {
			if ( ! is_page() ) {
				return false;
			}
			
			if ( is_page( $pid ) ) {
				return true;
			}
			
			/**
			 * The ID is not a number, so try to get the post by 
			 * 		its slug
			 */
			if ( ! is_numeric( $pid ) ) {
				$posts = get_posts( array( 
					'name' => $pid, 
					'post_type' => 'page', 
					'post_status' => 'publish', 
					'posts_per_page' => 1, 
				) );
				
				if ( ! is_wp_error( $posts ) && is_array( $posts ) ) {
					$pid = $posts[0]->ID;
				}
			}
			
			/**
			 * The ID is not a number, and we didn't find it by slug;
			 * 		try to search for it by title now
			 */
			if ( ! is_numeric( $pid ) ) {
				$posts = get_posts( array(
					'title' => $pid, 
					'post_type' => 'page', 
					'post_status' => 'publish', 
					'posts_per_page' => 1, 
				) );
				
				if ( ! is_wp_error( $posts ) && is_array( $posts ) ) {
					$pid = $posts[0]->ID;
				}
			}
			
			$anc = get_post_ancestors( $pid );
			foreach ( $anc as $ancestor ) {
				if ( is_page() && $ancestor == $pid ) {
					return true;
				}
			}
			
			return false; 
		}
	}
}
